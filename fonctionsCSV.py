import turtle as t
from os import walk

def get_fichiers(monRepertoire):
    listeFichiers = []
    for (repertoire, sousRepertoires, fichiers) in walk(monRepertoire):
        listeFichiers.extend(fichiers)
    while '.directory' in listeFichiers:
        listeFichiers.remove('.directory')
    return listeFichiers

# # # # # # # # # # # # # # # # # # # # # # #
def descfind(table :list):
    """
    prend en paramètre une table dont il retournera les descripteurs
    """
    desc = []
    for i in table[0]:
        desc += [i]
    return desc

def strtolist(texte):
        #Transforme les Str de types "[1,2,3,4]" en liste [1,2,3,4]
        texte = list(texte)
        while "'" in texte:
            texte.remove("'")
        newtexte = ""
        for i in texte:
            if i != " ":
                newtexte += str(i)
        newtexte = newtexte[1:-1]
        newtexte = newtexte.split(",")

        return newtexte


def multitable(table:list):
    #transforme un tableau contenant des listes en un truc plus propre
    #ex       PARTI 1 | ['Jo', 'Gab', 'Philou'] | ['17 pts', '9999 pts', '37 pts']
    #
    #Devient  PARTI 1 | Jo     | 17 pts
    #                 | Gab    | 9999 pts
    #                 | Philou | 37 pts
    #
    #de facto c'est plutôt: [{"Nom":Jo, "options":['NSI','MATH']} -> [{"Nom":Jo, "options":'NSI'},{"Nom":Jo, "options":'MATH'}]
    descripteurs = descfind(table) #on chope les descripteurs
    
    newtable = []
    for ligne in table: #on regarde chaque ligne
        littletable = [ligne]

        for desc in descripteurs: #puis chaque descripteur
            if str(ligne[desc])[0] == "[" and str(ligne[desc])[-1] == "]": #on repère un format liste
                listed = strtolist(str(ligne[desc])) #on le transforme en une vrai liste

                while len(littletable)<len(listed):
                    newligne = {}
                    for desc2 in descripteurs:
                        newligne[desc2] = ""
                    littletable += [newligne]

                for i in range(len(listed)):
                    littletable[i][desc] = str(listed[i])
        
        if littletable == []:
            newtable += [ligne]
        
        else:
            newtable += littletable
    
    table = list(newtable)
    return table


def affichage(tableutilisateur:list, afficher = "yes"):
    """
    Permet un affichage à la fois sexy et raffiné de la table csv mise en paramètres
    Prend en paramètre une table en format [{"truc":bidule1},{"truc":bidule2},{"truc":bidule3}] et "afficher" qui permet de directement print (ou non) l'affichage
    Retourne l'affichage en format STR
    """
    def maxlen(table3, numdesc):
        """Permet de connaître la longueur maximale d'une case dans une colonne donnée pour aligner l'affichage"""
        max = 0
        for ligne in table3:
            if len(str(ligne[numdesc])) > max:
                max = len(str(ligne[numdesc]))
        return max

    table = list(tableutilisateur) #éviter les alias
    descripteurs = descfind(table)

    texte = {}

    #ajoute la ligne des descripteurs à la table
    for i in descripteurs:
        texte[i] = i
    table = [texte] + table
    texte = ""

    table = multitable(table)
                

    for ligne in table:
        for desc in descripteurs:
            texte += str(ligne[desc]) + " "*(maxlen(table, desc)-len(str(ligne[desc]))) + " | "
        texte += "\n"

    if afficher == "yes":
        print(texte)
    return texte

def traiter(chemin:str, séparateur:str):
    """Permet d'ouvrir un fichier csv et dircetement le transformer en liste de dictionnaires. retourne la liste et les descripteurs de manière individuelle"""
    fichier = open(chemin, "r", encoding = "utf-8")
    table = fichier.read()
    fichier.close()
    
    if "\ufeff" == table[0]:
        table = table[1:]
        
    table = table.split("\n")
    while "" in table:
        table.remove("")
    
    tablefinal = []
    n = 0

    for ligne in table:
        ligne = ligne.split(séparateur)
        ligne = ligne[:-1]
        if n == 0:
            descripteurs = ligne
            n = 1
        else:
            valeur = {}
            n2 = 0
            for desc in ligne:
                valeur[descripteurs[n2]] = desc
                n2 += 1

            tablefinal += [valeur]

    return tablefinal

def jointure(table1:list, desc1:list, table2:list, desc2:list):
    table3 = []
    for ligne1 in table1:
        n = 0
        while ligne1["id"] != table2[n]["id"]:
            n+=1
        ligne2 = table2[n]

        ligne3 = ligne1
        for desc in ligne2:
            if desc not in ligne3:
                ligne3[desc] = ligne2[desc]

        table3 += [ligne3]

    for desc in desc2:
        if desc not in desc1:
            desc1 += [desc]
    return table3, desc1

def save(table:list, séparateur:str, fichier:str):
    """
    Permet de sauvegarder au format CSV un tableau de la forme [{"truc":bidule1},{"truc":bidule2},{"truc":bidule3}] en python
    """
    texte = ""
    descripteurs = descfind(table)
    for desc in descripteurs:
        texte += desc + séparateur
    texte += "\n"

    for ligne in table:
        for desc in ligne:
            texte += str(ligne[desc]) + séparateur
        texte+="\n"

    file = open(fichier, "w", encoding = "utf-8")
    file.write(texte)
    file.close()

def search(word, desc, table:list):
    #Permet de savoir si un mot (word) est dans la table (table) avec pour descripteur desc
    for ligne in table:
        if str(ligne[desc]) == str(word):
            return str(table.index(ligne))
    return False

def changedate(table:list, descdate, model = 'jour' ):
    #Change le format de la date _complète_ d'une table pour n'en garder que le jour, le mois, ou l'année et ce dans toute la table au descripteur descdate
    for ligne in table:
        if model == "jour":
            ligne[descdate] = ligne[descdate].split(" ")[1:]
            ligne[descdate].pop(2)
        
        elif model == "mois":
            ligne[descdate] = ligne[descdate].split(" ")[1:]
            ligne[descdate].pop(2).pop(1)
        
        elif model == "an":
            ligne[descdate] = ligne[descdate].split(" ")[-1]
        
        if type(ligne[descdate]) == list :
            temp = list(ligne[descdate])
            ligne[descdate] = ""
            for i in temp:
                ligne[descdate]+=i + " "
    return table

def max(liste:list):
    #!!!COMPARE DES ENTIERS!!!
    max = float(liste[0])
    for i in liste:
        if float(i)<0:
            i = -float(i)
        if float(i)>max:
            max = float(i)
    return max

def graphique(table:str, desc, descdate, titre, legendeX = "oui"):
    """
    Permet de dessiner un graphique avec turtle du descripteur (desc) de la table (table) en fonction de la date (descdate).
    Titre permet de donner un titre au graphique et legendX permet de préciser si l'on veut une légende des abscisse ou non
    C'est une procédure
    ATTENTION BUG : si l'on ferme le graphique et qu'on en refait un autre sans stopper le programme, le programme plante
    """
    table_save = table
    table = multitable(table)

    listevaleurs = []
    date = ""
    i = -1
    for ligne in table:
        if ligne[descdate] != date and ligne[descdate] != "":
            date = ligne[descdate]
            i+=1
            listevaleurs += [0]
        listevaleurs[i]+=float(ligne[desc])
    
    assert len(listevaleurs)>=2, "la liste des valeursest trop courte"
    
    diviseur = float(max(listevaleurs))/300
    x = -600
    y = listevaleurs[0]/diviseur

    t.setup()
    t.title(titre)
    
    #Dessine le repère
    t.pencolor("black")
    t.penup()
    t.goto(-600,-300)
    t.pendown()
    t.goto(-595,-300)
    t.goto(-600,-300)
    
    #la légende + le repère ordonnées
    t.setheading(90)
    for i in range(12):
        t.right(90)
        t.forward(5)
        t.write(str(round((float(max(listevaleurs)*(i-6))/6), 3)))
        t.backward(5)
        t.left(90)
        t.forward(50)
    t.right(90)
    t.forward(5)
    t.write(str(round(float(max(listevaleurs)), 3)))
    t.backward(5)
    
    t.penup()
    t.goto(-600,0)
    t.setheading(0)
    t.pendown()
    
    #dessine l'axe des abscisses
    if legendeX == "oui": 
        n = 0
        for i in range(len(listevaleurs)):
            t.left(90)
            t.backward(15)
            t.write(table_save[n][descdate])
            save = table_save[n][descdate]
            while n< len(table_save) and save == table_save[n][descdate]:
                n+=1
            t.forward(15)
            t.right(90)
            t.forward(1200/len(listevaleurs))
    else:
        t.goto(600, 0)
        
    #dessine la courbe
    t.penup()
    t.goto(x, y)
    t.pendown()
    t.pencolor("blue")
    for val in listevaleurs[1:]:
        x += 1200/len(listevaleurs)
        y = val/diviseur
        t.goto(x,y)
    t.exitonclick()

# # # # # # # # # # # # # # # # # # # # # # #
