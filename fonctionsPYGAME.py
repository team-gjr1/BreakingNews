import pygame

def display_texte(texte, fenetre, coordonnées = (0,0), size = 32, couleur = (0,0,0), background_color = None, centrer = True):
    font = pygame.font.Font('ressources/TIMES.ttf', size) #police d'écriture du jeu
    if background_color == None:
        display_texte = font.render(texte, True, couleur)
    else:
        display_texte = font.render(texte, True, couleur, background_color)

    rect_texte = display_texte.get_rect()
    if centrer:
        rect_texte.center = coordonnées
    else:
        rect_texte.x, rect_texte.y = coordonnées
    fenetre.blit(display_texte, rect_texte)


def input_pygame(texte, fenetre, size = 160):
    font = pygame.font.Font('ressources/TIMES.ttf', size) #police d'écriture du jeu
    texte_utilisateur = ""
    rect_input = pygame.Rect((fenetre.get_size()[0]//2-350, fenetre.get_size()[1]//2-80), (700, 180))

    constante = True
    while constante:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                return None

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    constante = False

                elif event.key == pygame.K_BACKSPACE:
                    # Stocker le texte sans la dernière lettre
                    texte_utilisateur = texte_utilisateur[0:-1]

                elif event.key not in (pygame.K_LSHIFT, pygame.K_RSHIFT, pygame.K_SEMICOLON):
                    if len(texte_utilisateur)<16:
                        texte_utilisateur += event.unicode


        pygame.draw.rect(fenetre, pygame.Color("gray15"), rect_input)
        surface_texte = font.render(texte_utilisateur, True, (255, 255, 255))
        fenetre.blit(surface_texte, ((fenetre.get_size()[0]-rect_input.w)//2, (fenetre.get_size()[1]-rect_input.h)//2))
        rect_input = pygame.Rect(((fenetre.get_size()[0]-rect_input.w)//2, (fenetre.get_size()[1]-rect_input.h)//2), (max(700, surface_texte.get_width() + 10), 180))

        pygame.display.update()
        fenetre.fill((255, 255, 255))
        fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0)) #on rajoute un joli fond d'écran
        display_texte(texte, fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2 -250), size = 100)

        if len(texte_utilisateur) == 16:
            display_texte("16 caractères maximum", fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2 +250), couleur = (255,0,0))


    return texte_utilisateur

def waiter():
    acceptation = False
    while not acceptation:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN or event.type == pygame.QUIT:
                acceptation = True