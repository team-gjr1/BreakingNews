from random import choice
from fonctionsPYGAME import *
import fonctionsCSV as csv
import fonctionsGAME

import pygame
from pygame.locals import *


def parcours_sauvegarde(racine):
    #créer une liste de noeuds en format csv (une liste de dicos) à partir d'un noeud raçine
    result = [] #la liste de dictionnaires que l'on va retourner
    pile_noeuds = [] #la pile de noeuds que l'on devra étudier
    id_used = [] #la liste des id des noeuds déjà enregistrés (il y a un game over infini dans le programme, et il tente d'enregistrer en boucle le même noeud)

    #on définit le noeud "racine"
    option_id = {} #d'abords on réduit ses options à des id
    for i in racine.option:
        if racine.option[i] != None:
            option_id[i] = racine.option[i].noeud_id

        else:
            option_id[i] = None
    
    result += [{
    "noeud_id" : racine.noeud_id,
    "option"   : option_id,
    "texte"    : racine.texte,
    "perso_id" : racine.perso_id,
    "scène_id" : racine.scene_id,
    "racine?" : True}]
    id_used += [racine.noeud_id]

    for i in racine.option:
            if racine.option[i] != None and racine.option[i].noeud_id not in id_used:
                pile_noeuds += [racine.option[i]] #on rajoute les options du noeuds étudié à la pile de noeuds
        

    while len(pile_noeuds) > 0: #tant qu'il y a des noeuds à étudier
        noeud = pile_noeuds.pop(-1)
        
        option_id = {}
        for i in noeud.option: #on étudit ses options
            if noeud.option[i] != None:
                option_id[i] = noeud.option[i].noeud_id

            else:
                option_id[i] = None

        result += [{
        "noeud_id" : noeud.noeud_id,
        "option"   : option_id,
        "texte"    : noeud.texte,
        "perso_id" : noeud.perso_id,
        "scène_id" : noeud.scene_id,
        "racine?" : False}]
        id_used += [noeud.noeud_id]

        for i in noeud.option:
            if noeud.option[i] != None and noeud.option[i].noeud_id not in id_used:
                pile_noeuds += [noeud.option[i]] #on rajoute les options du noeuds étudié à la pile de noeuds

    return result


def creernoeud(liste_csv):
    #renvoie un noeud racine à partir d'une liste_csv de format : [{noeud_id, option, texte, perso_id, scene_id, racine?}]
    #d'abord on cherche la ligne correspondant au noeud "racine" ('racine? == True)
    n = 0
    assert n < len(liste_csv), "IL N'Y A PAS DE RACINE"
    while not liste_csv[n]["racine?"]:
        n += 1
        assert n < len(liste_csv), "IL N'Y A PAS DE RACINE"

    racine = liste_csv[n]
    
    racine = Noeud(racine["noeud_id"], racine["option"], racine["texte"], racine["perso_id"], racine["scène_id"])

    #le concept c'est de créer une pile de noeuds dont on va chacuns leur tour étudier leurs options pour y mettre d'autres noeuds qu'on étudiera à leur tours
    pile_noeuds = [racine] 

    while len(pile_noeuds) > 0:
        last_one = pile_noeuds.pop(-1) #on prends le dernier élément de la pile

        for i in last_one.option: #on regarde ses options
            if type(last_one.option[i]) == str: #si ce sont des identifiants d'autres noeuds, on les cherche...
                n = 0
                while liste_csv[n]["noeud_id"] != last_one.option[i]:
                    n += 1
                    assert n < len(liste_csv), "Noeud Introuvable"
                
                #...et on les grand remplace
                last_one.option[i] = Noeud(liste_csv[n]["noeud_id"], liste_csv[n]["option"], liste_csv[n]["texte"], liste_csv[n]["perso_id"], liste_csv[n]["scène_id"])

                pile_noeuds += [last_one.option[i]] #et on rajoute le nouveau noeud à la pile

    return racine


def strrange(min: int, max:int):
    #créer une liste de nombre en str de min à max (non inclus)
    result = []
    for i in range(min,max):
        result += [str(i)]
    return result

class Noeud:
    def __init__(self, noeud_id, option: dict, texte: str, perso_id: str, scene_id: str):
        global liste_id
        global id

        if type(noeud_id) == list:
            alphabet = "abcdesfghijklmnopqrstuvwxyz0123456789"
            new_id = choice(alphabet)
            while new_id in noeud_id:
                new_id += choice(alphabet)
            noeud_id = new_id
        self.noeud_id = noeud_id
        
        self.option = option #un dictionnaire {"choix" : noeud_suivant}
        self.texte = texte
        self.perso_id = perso_id
        self.scene_id = scene_id

#FONCTIONS MUSICS
class DJ:
    def __init__(self, playlist):
        self.playlist = playlist
        self.music = True

    
    def change(self):
        if self.music:
            self.playlist += [self.playlist.pop(0)]
            pygame.mixer.music.load(self.playlist[0])
            pygame.mixer.music.play(-1)

    def silence(self):
        if pygame.mixer.music.get_busy():
            pygame.mixer.music.pause()
        else:
            pygame.mixer.music.unpause()


#------------------------------------
            
def calculer_taille_texte(texte, taille = 64):
    #donne une approximation de la taille du texte en pixel.
    result = 0
    for lettre in texte:
        if lettre in "AZERTYUIOPQSDFGHJKLMWXCVBN":
            result += 53*(taille/64)
        
        else:
            result += 27

    return result

class Game:
    def __init__(self, racine, nom_du_joueur, dj, liste_noeuds_id, racinePRIME):
        self.racine = racine #La raçine de l'arbre de jeu (type : Noeud)
        self.nom_du_joueur = nom_du_joueur
        self.dj = dj
        self.liste_noeuds_id = liste_noeuds_id
        self.racinePRIME = racinePRIME

    def jouer(self, fenetre, construire = False):
        curseur = self.racine
        curseur_save = curseur 

        curseur_choix = 0
        choix = None
        game_id = None
        #Boucle de Jeu
        while curseur != None :
            fenetre.fill((255, 255, 255))
            
            #Affichage de la scène
            if curseur.scene_id + ".png" in csv.get_fichiers("Images/Scene"):
                fenetre.blit(pygame.image.load("Images/Scene/" + curseur.scene_id + ".png").convert(), (0,0))
            else:
                fenetre.blit(pygame.image.load("Images/Scene/NotFound44.png").convert(), (0,0))
                print("Il manque la scène :", curseur.scene_id)

            #Affichage du Personnage
            if curseur.perso_id + ".png" in csv.get_fichiers("Images/Perso"):
                image = pygame.image.load("Images/Perso/" + curseur.perso_id + ".png").convert_alpha()
                fenetre.blit(image, (fenetre.get_size()[0] - image.get_size()[0],0))
            else:
                image = pygame.image.load("Images/Perso/NotFound44.png").convert_alpha()
                fenetre.blit(image, (fenetre.get_size()[0] - image.get_size()[0],0))
                print("Il manque le perso :", curseur.perso_id)
            
            #Le Texte
            blanc_artistique1 = pygame.Surface((fenetre.get_size()[0], fenetre.get_size()[1]-image.get_size()[1]-70))
            blanc_artistique1.fill((255,255,255))
            fenetre.blit(blanc_artistique1, (0, image.get_size()[1]+70))

            texte = curseur.texte
            while "=Nom=" in texte:
                texte = texte.replace("=Nom=", self.nom_du_joueur)
            
            #-------------------------- MINI-JEUX --------------------------------
            if texte[:6] == ":GAME=":
                if game_id == None:
                    game_id = texte[6:8]
                texte = texte[9:]

            liste_texte = [texte]
            while calculer_taille_texte(liste_texte[-1]) > fenetre.get_size()[0]:
                last_ligne = liste_texte[-1]
                liste_texte = liste_texte[:-1]

                liste_mots = last_ligne.split(" ")
                new_ligne = liste_mots.pop(0)

                while len(liste_mots)>0 and calculer_taille_texte(new_ligne + " " + liste_mots[0]) < fenetre.get_size()[0]:
                    new_ligne += " " + liste_mots.pop(0)
                
                ceux_qui_restent = []
                if len(liste_mots)>0:
                    ceux_qui_restent = liste_mots.pop(0)
                    while len(liste_mots)>0:
                        ceux_qui_restent += " " + liste_mots.pop(0)
                    
                liste_texte = liste_texte + [new_ligne] + [ceux_qui_restent]
            
            n = 0
            for ligne in liste_texte:
                display_texte(ligne, fenetre, (0, image.get_size()[1]+70+n*64), size = 64, centrer = False)
                n += 1


            #Nom du personnage
            blanc_artistique2 = pygame.Surface((377,70))
            blanc_artistique2.fill((255,255,255))
            fenetre.blit(blanc_artistique2, (fenetre.get_size()[0] - image.get_size()[0], image.get_size()[1]))
            display_texte(curseur.perso_id[:-2], fenetre, (fenetre.get_size()[0] - image.get_size()[0]//2, image.get_size()[1]+35), 64, background_color=(255,255,255))
            

            #Les Choix :
            if len(curseur.option)>0: #si il y a des options.
                
                #On adapte la taille des caractères des choix
                liste_taille = []
                for option in curseur.option:
                    t = 64 #la taille
                    x = len(option)
                    if x > 40:
                        x -= 40
                        t = 54
                        while x - 10 > 0:
                            t -= 4
                            x -= 10
                    liste_taille += [t]
                taille = min(liste_taille)
            
                n = 2
                liste_coo_choix = []
                for option in curseur.option: 
                    liste_coo_choix += [(64, image.get_size()[1]+60+64*(n-len(curseur.option)-2))]
                    display_texte(option, fenetre, liste_coo_choix[-1], taille, (255,255,255), centrer=False)
                    n += 1
                
                coo_curseur_choix = liste_coo_choix[curseur_choix%len(curseur.option)]
                fenetre.blit(pygame.image.load("Images/Menu/fleche.png").convert_alpha(), (coo_curseur_choix[0]-64, coo_curseur_choix[1]))
            pygame.display.update()

            ##########################
            #LANCEMENT DU MINIJEU
            if game_id != None and type(game_id) != bool:
                game_id = fonctionsGAME.play(game_id, fenetre)
            

            if game_id == "QUITTER":
                fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0))
                display_texte("SAUVEGARDE EN COURS...", fenetre, size = 96)
                pygame.display.update()
                curseur_save = curseur
                curseur = None

            if (curseur != None and len(curseur.option) == 0) or (type(game_id) == bool and not game_id): #si le joueur a choisi un noeud "game_over", sans options, ou s'il a perdu au mini-jeu
                #GAME OVER
                curseur = None

                print("GAME OVER")
                waiter()

                #MUSIC DE OUF POTO
                if self.dj.music:
                    pygame.mixer.music.load("Musics/NGGYU.mp3")
                    pygame.mixer.music.play(-1)

                fenetre.fill((0,0,0))
                display_texte("GAME OVER", fenetre, (fenetre.get_size()[0]//2,fenetre.get_size()[0]//2), 250, (126,0,0))
                pygame.display.update()
                waiter()

                self.dj.change()
            ##########################

           
            if (curseur != None and len(curseur.option) > 0) and choix != None: #si le noeud a des noeuds suivants et que le joueur a choisi un noeud
                

                #curseur.option[list(curseur.option.keys())[choix]] désigne le prochain curseur

                #Permet de construire plus facilement le jeu
                if curseur.option[list(curseur.option.keys())[choix]] == None: #si il n'y a pas de noeuds suivant 
                    
                    if construire: #et qu'on peut construire
                        texte = input("Texte (#QUITTER) : ")
                        
                        if texte != "#QUITTER":
                            nb_option = input("nb_option : ")
                            perso_id = input("perso_id : ")
                            assert "," not in perso_id, "PAS DE ',' DANS LES ID"
                            
                            scene_id = input("scene_id : ")
                            assert "," not in scene_id, "PAS DE ',' DANS LES ID"

                            new_option = {}
                            for i in range(int(nb_option)):
                                nom_option = input("option " + str(i+1) + " : ")
                                new_option[nom_option] = None

                            curseur.option[list(curseur.option.keys())[choix]] = Noeud(self.liste_noeuds_id,  new_option, texte, perso_id, scene_id)
                            self.liste_noeuds_id += [curseur.option[list(curseur.option.keys())[choix]].noeud_id]
                        
                        else : #le dev peut quitter le jeu si fausse manip
                            fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0))
                            display_texte("SAUVEGARDE EN COURS...", coordonnées=(fenetre.get_size()[0]//2, fenetre.get_size()[1]//2), fenetre=fenetre, size = 96)
                            pygame.display.update()
                            curseur_save = curseur
                            curseur = None
                    
                    else:  #sinon on indique au joueur que les devs n'ont pas fini leur jeu (ces délinquants)
                        X, Y = fenetre.get_size()
                        fenetre.blit(pygame.image.load("Images/Scene/ENCONSTRUCTION.png").convert(), (X-1536,0))
                        pygame.display.update()
                        waiter()
                        curseur = None
                
                else:
                    curseur = curseur.option[list(curseur.option.keys())[choix]] #On passe au noeud suivant
                    choix = None
                    game_id = None
                        

            pygame.display.update()
            if curseur != None:
                for event in pygame.event.get():
                    if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:
                            fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0))
                            display_texte("SAUVEGARDE EN COURS...", coordonnées=(fenetre.get_size()[0]//2, fenetre.get_size()[1]//2), fenetre=fenetre, size = 96)
                            pygame.display.update()
                            curseur_save = curseur
                            curseur = None

                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            curseur_choix += 1
                        
                        elif event.key == pygame.K_DOWN:
                            curseur_choix -= 1
                        
                        elif event.key == pygame.K_SPACE or event.key == pygame.K_RETURN:
                            choix = curseur_choix%len(curseur.option)
                    
                    
                        
        #-------]FIN DE LA BOUCLE[-------
        
        #on sauvegarde l'arbre de choix s'il a été modifié en mode construction 
        if construire:
            tableau = parcours_sauvegarde(self.racinePRIME)

            #on remplace les , par %,% pour pouvoir les distinguer lors du processus de récupération
            for ligne in tableau:
                #Incroyablement peu optimisé, mais beaucoup plus simple pour interpréter les tableaux ' -> ztefuzuerru123256 -> %,%
                for i in ligne["option"]:
                    while "," in i:
                        ligne["option"][i.replace(",", "ztefuzuerru123256")] = ligne["option"][i]
                        ligne["option"].pop(i)
                
                for i in ligne["option"]:
                    while "ztefuzuerru123256" in i:
                        ligne["option"][i.replace("ztefuzuerru123256", "%,%")] = ligne["option"][i]
                        ligne["option"].pop(i)
                    
                
                while "," in ligne["texte"]:
                    ligne["texte"] = ligne["texte"].replace(",", "ztefuzuerru123256")
                
                while "ztefuzuerru123256" in ligne["texte"]:
                    ligne["texte"] = ligne["texte"].replace("ztefuzuerru123256", "%,%")
            
            #on sauvegarde dans un fichier
            csv.save(tableau, "%;%", "Configs/configs.csv")
        
        return curseur_save


def MAIN(construire = False):
    ###------- MENU ------
    print("--------------------------\n1: Continuer\n2: Nouvelle Partie\n3: Quitter")

    #Démarrage de Pygame
    pygame.init()
    pygame.mixer.init()

    fenetre = pygame.display.set_mode((0,0)) #fenêtre d'affichage
    X, Y = fenetre.get_size()
    print(X,Y)
    pygame.display.set_caption('[Insérer Nom du Jeu]') #nom de la fenêtre

    #let's play the music
    fichiers = csv.get_fichiers("Musics/main_playlist")
    playlist = []
    for musique in fichiers:
        playlist += ["Musics/main_playlist/" + musique] 
    main_dj = DJ(playlist)
    main_dj.change() 

    choix = None
    curseur = 0
    while choix != 3:
        fenetre.fill((255, 255, 255)) #on colorie la fenêtre en blanc
        fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0)) #on rajoute un joli fond d'écran
        display_texte("Breaking News", fenetre, (X//2, Y//6), 128)

        coo_choix = [(X//2, Y//3), (X//2, Y//2), (X//2, 2*(Y//3)), (X//2, 5*(Y//6))]
        display_texte("Continuer", fenetre, coo_choix[0],64)
        display_texte("Nouvelle Partie", fenetre, coo_choix[1],64)
        display_texte("Options", fenetre, coo_choix[2],64)
        display_texte("Quitter", fenetre, coo_choix[3],64)

        image_curseur = pygame.image.load("Images/Menu/curseur.png").convert_alpha()
        fenetre.blit(image_curseur, (coo_choix[curseur%4][0]+200-75*(curseur%4-2), coo_choix[curseur%4][1]-image_curseur.get_size()[1]))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                choix = 3
            
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    curseur -= 1
                elif event.key == pygame.K_DOWN:
                    curseur += 1 
                
                elif event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                    choix = curseur%4

        pygame.display.update()

        #print("--------------------------\n1: Continuer\n2: Nouvelle Partie\n3: Quitter")
        #choix = input("?: ")

        if choix == 0 or choix == 1:
            #On traite les fichiers de configuration
            tableau = csv.traiter("Configs/configs.csv", "%;%")

            #le format csv ne retourne que des STR, on doit donc adapter certaines colonnes pour leur donner le type voulu
            id_racine_originale = None
            liste_noeuds_id = []
            for ligne in tableau:
                #on fait la liste des noeuds_id existants
                liste_noeuds_id += [ligne["noeud_id"]]
                #racine? en bool + recherche de la racine originale (le premier noeud du jeu)
                if ligne["racine?"] == "True":
                    ligne["racine?"] = True
                    id_racine_originale = ligne["noeud_id"]
                else:
                    ligne["racine?"] = False
                

                #option en dict
                intermed = ligne["option"]
                result = {}
                if intermed != '{}':
                    intermed = intermed[1:-1] #on retire les {}
                    intermed = intermed.split(", ")

                    
                    for part in intermed:
                        clé = part.split(": ")[0][1:-1]

                        while "%,%" in clé:
                            clé = clé.replace("%,%", ",")
                        
                        result[clé] = part.split(": ")[1]

                        if result[clé] == "None":
                            result[clé] = None
                        
                        else:
                            result[clé] = result[clé][1:-1]
                
                ligne["option"] = result

                #on remplace les %,% par des , dans le texte
                while "%,%" in ligne["texte"]:
                    ligne["texte"] = ligne["texte"].replace("%,%", ",")
            
            assert id_racine_originale != None, "Les fichiers du jeu sont corrompus."
            

            nom_du_joueur = None
            if choix == 0 : #Si c'est "continuer" on doit reconfigurer le tableau pour que la racine soit le noeud où la partie précédente s'est arrêtée
                texte = open("Configs/sauvegarde.txt", "r", encoding = "utf-8").read().split(";")

                nom_du_joueur = texte[0]
                id_racine = texte[1]
                choix = None
                
            elif choix == 1: #NOUVELLE PARTIE
                nom_du_joueur = input_pygame("Quel est votre nom ?", fenetre)
                
                id_racine = id_racine_originale
                choix = None
            
            if nom_du_joueur != None:
                racinePRIME = creernoeud(tableau)
                for ligne in tableau:
                    if ligne["noeud_id"] == id_racine:
                        ligne["racine?"] = True
                    else:
                        ligne["racine?"] = False

                
                racine = creernoeud(tableau)
                noeud_sauvegarde = Game(racine, nom_du_joueur, main_dj, liste_noeuds_id, racinePRIME).jouer(fenetre, construire) #le noeud où le joueur a arrêté de jouer

                file = open("Configs/sauvegarde.txt", "w", encoding = "utf-8")
                file.write(nom_du_joueur + ";" + noeud_sauvegarde.noeud_id)
                file.close()
        
        if choix == 2 :
            curseur = 0
            choix_options = None
            while choix == 2: #OPTIONs
                fenetre.fill((255, 255, 255))
                fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0))
                display_texte("OPTIONS", fenetre, (X//2, Y//6), 128)

                coo_choix = [(X//2, Y//3), (X//2, Y//2), (X//2, 2*(Y//3))]
                
                display_texte("Aide", fenetre, coo_choix[0],64)
                if main_dj.music:
                    display_texte("Music : ON", fenetre, coo_choix[1],64)
                else:
                    display_texte("Music : OFF", fenetre, coo_choix[1],64)

                display_texte("Retour", fenetre, coo_choix[2],64)

                image_curseur = pygame.image.load("Images/Menu/curseur.png").convert_alpha()
                fenetre.blit(image_curseur, (coo_choix[curseur%3][0]+200-75*(curseur%3-2), coo_choix[curseur%3][1]-image_curseur.get_size()[1]))

                pygame.display.update()

                for event in pygame.event.get():
                    if (event.type == pygame.KEYDOWN and event.key ==pygame. K_ESCAPE) or event.type == pygame.QUIT:
                        choix = None
                    
                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            curseur -= 1

                        elif event.key == pygame.K_DOWN:
                            curseur += 1 

                        elif event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                            choix_options = curseur%3
                
                if choix_options == 2:
                    choix = None
                
                elif choix_options == 1:
                    main_dj.music = not main_dj.music
                    choix_options = None

                    main_dj.silence()

                
                elif choix_options == 0:
                    fenetre.blit(pygame.image.load("Images/Menu/ossecour.png").convert(), (0,0))
                    pygame.display.update()
                    waiter()
                    choix_options = None
    
    pygame.quit()
        
MAIN(construire = False) #Mettre True pour continuer à construire le jeu (ne pas oublier de regarder la console)
