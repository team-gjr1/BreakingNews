from fonctionsPYGAME import *
import pygame
import random


def text_writer(difficulté, fenetre): #super nom de mini-jeu
    fichier = open("ressources/liste_mots.txt", "r", encoding = "UTF-8")
    liste_mots = fichier.read().split("\n")[:-1]

    nb_mots = 10
    font = pygame.font.Font('ressources/TIMES.ttf', 144) #Police d'écriture

    pts = 0
    while nb_mots > 0:
        mot_choisi = random.choice(liste_mots)
        liste_mots.remove(mot_choisi)

        rect_game_black = pygame.Rect((fenetre.get_size()[0]//6-25, fenetre.get_size()[1]//6-25), ((fenetre.get_size()[0]//3)*2+50, (fenetre.get_size()[1]//3)*2+50))
        rect_game_white = pygame.Rect((fenetre.get_size()[0]//6, fenetre.get_size()[1]//6), ((fenetre.get_size()[0]//3)*2, (fenetre.get_size()[1]//3)*2))
        rect_input = pygame.Rect((fenetre.get_size()[0]//2-350, fenetre.get_size()[1]//2-80), (700, 160))

        texte_utilisateur = ""
        constante = True
        time = pygame.time.get_ticks()
        while constante and pygame.time.get_ticks() - time <= 5000:
            for event in pygame.event.get():
                if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    return "QUITTER"

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        constante = False

                    elif event.key == pygame.K_BACKSPACE:
                        # Stocker le texte sans la dernière lettre
                        texte_utilisateur = texte_utilisateur[0:-1]

                    elif event.key not in (pygame.K_LSHIFT, pygame.K_RSHIFT, pygame.K_SEMICOLON):
                        if len(texte_utilisateur)<16:
                            texte_utilisateur += event.unicode

            fenetre.fill((255, 255, 255))
            fenetre.blit(pygame.image.load("Images/Menu/fond.png").convert(), (0,0)) #on rajoute un joli fond d'écran

            pygame.draw.rect(fenetre, pygame.Color("black"), rect_game_black)
            pygame.draw.rect(fenetre, pygame.Color("white"), rect_game_white)
            pygame.draw.rect(fenetre, pygame.Color("gray15"), rect_input)

            display_texte(str(pts)+"/10", fenetre, ((fenetre.get_size()[0]//6)*5-108, fenetre.get_size()[1]//6+36), size = 36)
            display_texte(str(pygame.time.get_ticks() - time)[:-3], fenetre, (fenetre.get_size()[0]//6+74, fenetre.get_size()[1]//6+36), size = 36)
            display_texte(str(11-nb_mots), fenetre, ((fenetre.get_size()[0]//6)*5-108, fenetre.get_size()[1]//6*5-36), size = 36)
            display_texte("ECRIS !!!!!", fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2 -225), size = 100)
            display_texte(mot_choisi, fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2 -125), size = 90)

            surface_texte = font.render(texte_utilisateur, True, (255, 255, 255))
            fenetre.blit(surface_texte, ((fenetre.get_size()[0]-surface_texte.get_size()[0])//2, (fenetre.get_size()[1]-surface_texte.get_size()[1])//2))
            rect_input = pygame.Rect(((fenetre.get_size()[0]-rect_input.w)//2, (fenetre.get_size()[1]-rect_input.h)//2), (max(700, surface_texte.get_width() + 10), 160))

            pygame.display.update()


        if mot_choisi == texte_utilisateur :
            fenetre.fill((0, 255, 0))
            display_texte("OUI", fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2), size = 850, couleur=(0,200,0))
            pts += 1

        elif texte_utilisateur == "passer jeu":
            nb_mots = 0
            pts = 10


        else:
            fenetre.fill((255, 0, 0))
            display_texte("NON", fenetre, (fenetre.get_size()[0]//2, fenetre.get_size()[1]//2), size = 850, couleur=(200,0,0))
        pygame.display.update()
        pygame.time.wait(1000)
        nb_mots -= 1

    if pts >= difficulté:
        return True
    return False


def facteur(difficulté, fenetre):
    #difficulté représente le nombre de lettres à distribuer en 30s
    
    couleurs = ["bleu", "rouge", "vert", "jaune"]
    
    coo_side = [(fenetre.get_size()[0]//2, fenetre.get_size()[1]//10), (fenetre.get_size()[0]//6*5, fenetre.get_size()[1]//2), (fenetre.get_size()[0]//2, fenetre.get_size()[1]//10*9), (fenetre.get_size()[0]//6, fenetre.get_size()[1]//2)]
    side = [] #les couleurs de chaque cotés sont chosisis aléatoirements
    while len(couleurs)>0:
        side += [couleurs.pop(random.randint(0,len(couleurs)-1))]
    
    #[HAUT, DROITE, BAS, GAUCHE]

    #FOND
    fenetre.fill((255,255,255)) #le fond blanc
    for i in range(len(side)): #les rectangles
        fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/rectangle" + side[i] + ".png").convert_alpha(), (coo_side[i][0]-325//2, coo_side[i][1]-325//2))

    

    couleurs = ["bleu", "rouge", "vert", "jaune"]
    cheat_count = 0
    #la boucle de jeu ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~é
    time = pygame.time.get_ticks()
    while difficulté > 0 and pygame.time.get_ticks() - time <= 30000:
        couleur_lettre = random.choice(couleurs) #on choisi une couleur de lettre aléatoire
        type_lettre = random.choice(["journal", "colis", "enveloppe"]) 

        fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/" + type_lettre + couleur_lettre + ".png").convert_alpha(), (fenetre.get_size()[0]//2-150, fenetre.get_size()[1]//2-150))

        pygame.display.update()
        choix = True
        while type(choix) == bool and pygame.time.get_ticks() - time <= 30000:
            pygame.draw.rect(fenetre, pygame.Color("white"), pygame.Rect((fenetre.get_size()[0]//6-150, fenetre.get_size()[1]//6-50), (300, 100)))
            display_texte(str(round((30000 - (pygame.time.get_ticks() - time))/1000))+"s || "+str(difficulté), fenetre, (fenetre.get_size()[0]//6, fenetre.get_size()[1]//6), size = 64, couleur=(200,0,0))
            
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    return "QUITTER"
                
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        choix = 0
                        cheat_count = 0

                    elif event.key == pygame.K_RIGHT:
                        choix = 1
                        cheat_count = 0
                       
                    
                    elif event.key == pygame.K_DOWN:
                        choix = 2
                        cheat_count = 0

                    elif event.key == pygame.K_LEFT:
                        choix = 3
                        cheat_count += 1
        
        if pygame.time.get_ticks() - time <= 30000:
            if couleur_lettre == side[choix]:
                fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/" + type_lettre + couleur_lettre + ".png").convert_alpha(), (coo_side[choix][0]-150, coo_side[choix][1]-150))
                fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/blanc_artistique.png").convert(), (fenetre.get_size()[0]//2-150, fenetre.get_size()[1]//2-150))

                difficulté -= 1
            
            else:
                fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/non.png").convert_alpha(), (fenetre.get_size()[0]//2-113, fenetre.get_size()[1]//2-150))
                pygame.display.update()
                pygame.time.wait(500)
                fenetre.blit(pygame.image.load("ressources/ressources_jeu_facteur/blanc_artistique.png").convert(), (fenetre.get_size()[0]//2-150, fenetre.get_size()[1]//2-150))

                difficulté += 3
        if cheat_count == 25:
            return True
    if difficulté > 0:
        return False #GAME OVER
    return True

def play(identifiant, fenetre):
    if identifiant == "01":
        return text_writer(7, fenetre)
        
    elif identifiant == "11":
        return facteur(25, fenetre)
    
    elif identifiant == "12":
        return facteur(35, fenetre)
    
    elif identifiant == "13":
        return facteur(50, fenetre)
